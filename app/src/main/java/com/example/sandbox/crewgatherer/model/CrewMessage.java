package com.example.sandbox.crewgatherer.model;

public class CrewMessage {
    private String message;

    public CrewMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
