package com.example.sandbox.crewgatherer.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "rooms")
public class Room {
    @DatabaseField(id = true)
    private int roomID;
    private String roomTitle;
    private String roomPictureUrl;


    public Room() {
    }

    public Room(int roomID) {
        this.roomID = roomID;
    }

    public int getRoomID() {
        return roomID;
    }

    public String getRoomTitle() {
        return roomTitle;
    }

    public String getRoomPictureUrl() {
        return roomPictureUrl;
    }
}
