package com.example.sandbox.crewgatherer.webservices;

public class RoomRequest {
    private String password; //TODO rozwa�y� generyczn� klase ��dania
    private String userName;
    private String userToken;
    private int roomID;
    private String roomName;
    //private String userToken;


    public RoomRequest(String password, String userName, int roomID, String roomName, String userToken) {
        this.password = password;
        this.userName = userName;
        this.roomID = roomID;
        this.roomName = roomName;
        this.userToken = userToken;
    }
}
