package com.example.sandbox.crewgatherer.webservices;

import com.example.sandbox.crewgatherer.model.ChannelRequest;
import com.example.sandbox.crewgatherer.model.Room;
import com.example.sandbox.crewgatherer.model.SendMessageRequest;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Webservice {

    @FormUrlEncoded
    @POST("getRooms.php")
    Single<List<Room>> getRoomsList(@Field("password") String password);

    @FormUrlEncoded
    @POST("register_gcm.php")
    Single<Response<ResponseBody>> storeRegId(@Field("reg_id") String reg_id, @Field("userName") String userName);

    @POST("send_gcm.php")
    Single<Response<ResponseBody>> sendMessage(@Body SendMessageRequest sendMessageRequest);

    @POST("createChannel.php")
    Single<ChannelResponse> createChannel(@Body ChannelRequest channelRequest);

    @POST("subscribeToChannel.php")
    Single<ChannelResponse> subscribeToChannel(@Body ChannelRequest channelRequest);

    @POST("unsubscribeFromChannel.php")
    Single<ChannelResponse> unsubscribeFromChannel(@Body ChannelRequest channelRequest);

    @POST("subscribeToRoom.php")
    Single<ChannelResponse> subscribeToRoom(@Body RoomRequest roomRequest);

    @POST("subscribeToRoom.php")
    Single<ChannelResponse> unsubscribeFromRoom(@Body RoomRequest roomRequest);
}
