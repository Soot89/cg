package com.example.sandbox.crewgatherer.presentation.roomsView;

import com.annimon.stream.Stream;
import com.example.sandbox.crewgatherer.data.store.RoomRepository;
import com.example.sandbox.crewgatherer.model.Room;
import com.example.sandbox.crewgatherer.model.SendMessageRequest;
import com.example.sandbox.crewgatherer.model.SendRoomMessage;
import com.example.sandbox.crewgatherer.utils.SharedPrefUtil;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;
import com.example.sandbox.crewgatherer.webservices.ChannelResponse;
import com.example.sandbox.crewgatherer.webservices.RoomRequest;
import com.example.sandbox.crewgatherer.webservices.Webservice;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class RoomsViewModel extends ViewModel {

    @NonNull
    private final Webservice mWebservice;
    @NonNull
    private SharedPrefUtil mSharedPrefUtil;

    private MutableLiveData<List<Room>> roomsListLiveData = new MutableLiveData<>();

    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private SingleLiveEvent<Boolean> mIsRoomSubscribed = new SingleLiveEvent<>();
    private SingleLiveEvent<ChannelResponse> mChannelResponseEvent = new SingleLiveEvent<>();

    @NonNull
    private RoomRepository mRoomRepository;

    @Nullable
    private Room mSelectedRoom;

    @Inject
    RoomsViewModel(@NonNull Webservice webservice, @NonNull RoomRepository roomRepository, @NonNull SharedPrefUtil sharedPrefUtil) {
        this.mWebservice = webservice;
        this.mRoomRepository = roomRepository;
        this.mSharedPrefUtil = sharedPrefUtil;
    }

    public void onLoad() {
        mCompositeDisposable.add(mWebservice.getRoomsList("SICRULEZ").observeOn(Schedulers.computation()).subscribeOn(Schedulers.io())
                .subscribe(rooms -> roomsListLiveData.postValue(rooms), Throwable::printStackTrace));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mCompositeDisposable.clear();
        mSelectedRoom = null;
    }


    public void isRoomSubscribed(Room room) {
        mSelectedRoom = room;
        boolean isRoomSubscribed = Stream.of(mRoomRepository.getSubscribedRooms()).anyMatch(roomSub -> roomSub.getRoomID() == mSelectedRoom.getRoomID());
        mIsRoomSubscribed.postValue(isRoomSubscribed);
        //if is subscribed - show dialog to SendMessage else show dialog with question about subscribing. If Yes -> subsribeToRoom()
    }

    public MutableLiveData<List<Room>> getRoomsListLiveData() {
        return roomsListLiveData;
    }

    public SingleLiveEvent<Boolean> getIsRoomSubscribed() {
        return mIsRoomSubscribed;
    }

    public void subscribeToRoom() {
        mCompositeDisposable.add(mWebservice.subscribeToRoom(new RoomRequest("SICRULEZ", mSharedPrefUtil.getUserName(), mSelectedRoom.getRoomID(), mSelectedRoom.getRoomTitle(), mSharedPrefUtil.getToken())).observeOn(Schedulers.computation()).subscribeOn(Schedulers.io())
                .subscribe(channelResponse -> {
                    if (channelResponse.getMessage() == null || channelResponse.getMessage().isEmpty()) {
                        mRoomRepository.insert(mSelectedRoom);
                    } else {
                       mChannelResponseEvent.postValue(channelResponse);
                    }
                }));
    }

    public void sendRoomMessage(SendRoomMessage sendRoomMessage) {
        mCompositeDisposable.add(mWebservice.sendMessage(new SendMessageRequest(mSelectedRoom.getRoomID(), true, sendRoomMessage.getMessage(), mSharedPrefUtil.getUserName(), "SICRULEZ"))
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.io())
                .subscribe(responseBodyResponse -> {
                    mChannelResponseEvent.postValue(new ChannelResponse("Wys�ano"));
                    Log.i("SendMsg", "Message sent");
                }, Throwable::printStackTrace));
    }

    public SingleLiveEvent<ChannelResponse> getChannelResponseEvent() {
        return mChannelResponseEvent;
    }
}
