package com.example.sandbox.crewgatherer.di.modules;

import com.example.sandbox.crewgatherer.ViewModelUtil;
import com.example.sandbox.crewgatherer.presentation.roomsView.RoomsViewModel;

import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class RoomsViewModelModule {
    @Singleton
    @Provides
    @Named("rooms")
    static ViewModelProvider.Factory provideViewModelProviderFactory(ViewModelUtil viewModelUtil, RoomsViewModel viewModel) {
        return viewModelUtil.createFor(viewModel);
    }
}
