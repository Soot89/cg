package com.example.sandbox.crewgatherer.presentation.roomsView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.RoomListActivityBinding;
import com.example.sandbox.crewgatherer.di.modules.ConnectivityModule;
import com.example.sandbox.crewgatherer.firebase.QuickstartPreferences;
import com.example.sandbox.crewgatherer.firebase.RegistrationIntentService;
import com.example.sandbox.crewgatherer.model.SendRoomMessage;
import com.example.sandbox.crewgatherer.presentation.SendMessageDialog;
import com.example.sandbox.crewgatherer.presentation.channelsView.ChannelsActivity;
import com.example.sandbox.crewgatherer.utils.SharedPrefUtil;

import android.arch.lifecycle.ViewModelProvider;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Named;

public class RoomsListActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1432514;
    private RoomListActivityBinding mBinding;

    private final RoomsAdapter mAdapter = new RoomsAdapter();

    @Inject
    @Named("rooms")
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    SharedPrefUtil mSharedPrefUtil;

    private RoomsViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((CrewGathererApplication) getApplication())
                .mApplicationComponent
                .newContextComponent(new ConnectivityModule(this))
                .inject(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.room_list_activity);
        viewModel = viewModelFactory.create(RoomsViewModel.class);

        mBinding.roomsGrid.setEnabled(mSharedPrefUtil.getUserName() != null);

        viewModel.onLoad();

        mBinding.roomsGrid.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.roomsGrid.setAdapter(mAdapter);

        mAdapter.getOnRoomClickListenerSubject().observe(this, room -> {
            Intent intent = new Intent(this, ChannelsActivity.class);
            intent.putExtra("RoomID", room.getRoomID());
            intent.putExtra("RoomName", room.getRoomTitle());
            startActivity(intent);
        });

        mAdapter.getOnRoomDoubleClickListenerSubject().observe(this, room -> viewModel.isRoomSubscribed(room));

        viewModel.getRoomsListLiveData().observe(this, rooms -> {
            mAdapter.setItemsList(rooms);
            mAdapter.notifyDataSetChanged();
        });

        viewModel.getChannelResponseEvent().observe(this, channelResponse -> Toast.makeText(this, channelResponse.getMessage(), Toast.LENGTH_SHORT).show());

        viewModel.getIsRoomSubscribed().observe(this, isSubscribed -> {
            if (isSubscribed) {
                SendMessageDialog sendMessageDialog = new SendMessageDialog();
                sendMessageDialog.getMessageSendSubject().observe(this, crewMessage -> viewModel.sendRoomMessage(new SendRoomMessage(crewMessage.getMessage())));
                sendMessageDialog.show(getSupportFragmentManager(), "SendMsg");
            } else {
                new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                        .setTitle("Subskrypcja do pokoju")
                        .setMessage("Nie jeste� zapisany do pokoju. Czy chcesz to zrobi� teraz?")
                        .setPositiveButton("Tak", (dialog, which) -> {
                            viewModel.subscribeToRoom();
                            dialog.dismiss();
                        })
                        .setNegativeButton("Nie", (dialog, which) -> dialog.dismiss())
                        .create().show();
            }
        });

        runGMC();
    }

    private void runGMC() {
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("Activity", "Received broadcast token refreshed");
                RegisterUserDialog registerUserDialog = new RegisterUserDialog();
                registerUserDialog.getRegisterUserSubject().subscribe(userName -> {
                    mSharedPrefUtil.saveUserName(userName);
                    mBinding.roomsGrid.setEnabled(mSharedPrefUtil.getUserName() != null);
                    Intent service = new Intent(context, RegistrationIntentService.class);
                    service.putExtra("USERNAME", userName);
                    startService(service);
                });
                registerUserDialog.show(getSupportFragmentManager(), "REGISTER");
            }
        }, new IntentFilter(QuickstartPreferences.TOKEN_RFRESHED));

        if (!checkPlayServices()) {
            Toast.makeText(this, "Brak Google Play Services", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(RoomsListActivity.class.getSimpleName(), "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}
