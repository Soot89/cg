package com.example.sandbox.crewgatherer.di.modules;

import dagger.Module;

@Module(includes = {RoomsViewModelModule.class, ChannelsViewModelModule.class})
public final class ViewModelModule {
}
