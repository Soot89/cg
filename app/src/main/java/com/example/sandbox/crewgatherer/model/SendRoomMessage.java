package com.example.sandbox.crewgatherer.model;

public class SendRoomMessage {
    private String message;

    public SendRoomMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
