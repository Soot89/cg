package com.example.sandbox.crewgatherer.di.modules;

import android.content.Context;


import com.example.sandbox.crewgatherer.di.ContextScope.ContextScope;
import com.example.sandbox.crewgatherer.utils.ConnectivityUtil;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectivityModule {
    private ConnectivityUtil connectivityUtil;

    public ConnectivityModule(Context context) {
        this.connectivityUtil = new ConnectivityUtil(context);
    }

    @Provides
    @ContextScope
    public ConnectivityUtil providesConnectivityUtil() {
        return connectivityUtil;
    }
}
