package com.example.sandbox.crewgatherer.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class SendMessageRequest {
    @Nullable
    private Integer channelID; //not always
    @NonNull
    private int roomID; //always needed
    @NonNull
    private boolean isRoomMessage; //For channel only message = false, for general/room message = true
    @NonNull
    private String message; //typed by user
    @NonNull
    private String sender; //userName obtained from SharedPrefs
    private String password;

    public SendMessageRequest(@Nullable Integer channelID, @NonNull int roomID, @NonNull boolean isRoomMessage, @NonNull String message, @NonNull String userName, String password) {
        this.channelID = channelID;
        this.roomID = roomID;
        this.isRoomMessage = isRoomMessage;
        this.message = message;
        this.sender = userName;
        this.password = password;
    }

    public SendMessageRequest(@NonNull int roomID, @NonNull boolean isRoomMessage, @NonNull String message, @NonNull String userName, String password) {
        this.roomID = roomID;
        this.isRoomMessage = isRoomMessage;
        this.message = message;
        this.sender = userName;
        this.password = password;
    }
}
