package com.example.sandbox.crewgatherer.model;

import com.example.sandbox.crewgatherer.webservices.RoomRequest;

public class ChannelRequest extends RoomRequest {
    private String channelName;

    public ChannelRequest(String password, String userName, int roomID, String roomName, String channelName, String userToken) {
        super(password, userName, roomID, roomName, userToken);
        this.channelName = channelName;
    }
}
