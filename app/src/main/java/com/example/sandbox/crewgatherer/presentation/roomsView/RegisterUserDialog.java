package com.example.sandbox.crewgatherer.presentation.roomsView;

import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.RegisterUserDialogBinding;

import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;

import io.reactivex.subjects.PublishSubject;

public class RegisterUserDialog extends DialogFragment {
    private RegisterUserDialogBinding binding;
    private PublishSubject<String> registerUserSubject = PublishSubject.create();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.i("RegisterDialog", "OnCreateDialog");

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.register_user_dialog, null, false );
        AlertDialog alertDialog =  new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                .setView(binding.getRoot())
                .setTitle("Zarejestruj si�")
                .setPositiveButton("OK",null)
                .setCancelable(false)
                .create();

        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setOnShowListener(dialog -> {
            Button positive = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            positive.setOnClickListener((view) -> onOkButtonClick(dialog));
        });

        return alertDialog;
    }

    private void onOkButtonClick(DialogInterface dialogInterface) {
        String userName = binding.userName.getText().toString();
        if (!userName.isEmpty()) {
            registerUserSubject.onNext(userName);
            dialogInterface.dismiss();
        } else {
            binding.userName.setError("Nazwa nie mo�e by� pusta");
        }
    }

    public PublishSubject<String> getRegisterUserSubject() {
        return registerUserSubject;
    }
}
