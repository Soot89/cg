package com.example.sandbox.crewgatherer.firebase;

public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String TOKEN = "token";
    public static final String TOKEN_RFRESHED = "token_refreshed";
    public static final String USER_NAME_KEY = "username";

}
