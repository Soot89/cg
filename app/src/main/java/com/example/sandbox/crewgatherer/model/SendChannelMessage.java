package com.example.sandbox.crewgatherer.model;

public class SendChannelMessage {
    private Channel mChannel;
    private String message;

    public SendChannelMessage(Channel channel, String message) {
        mChannel = channel;
        this.message = message;
    }

    public Channel getChannel() {
        return mChannel;
    }

    public String getMessage() {
        return message;
    }
}
