package com.example.sandbox.crewgatherer.presentation;

import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.model.Channel;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

public class RemoveSubscriptionDialog extends AppCompatDialogFragment {

    private Channel selectedChannel;
    private SingleLiveEvent<Channel> onRemoveChannel = new SingleLiveEvent<>();

    public void setSelectedChannel(Channel selectedChannel) {
        this.selectedChannel = selectedChannel;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog alertDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                .setTitle(R.string.removeSubscription)
                .setMessage(String.format(getString(R.string.remove_subscription_message), selectedChannel.getChannelName()))
                .setPositiveButton("Tak", (dialog, which) -> {
                    onRemoveChannel.postValue(selectedChannel);
                    dialog.dismiss();
                })
                .setNegativeButton("Anuluj", (dialog, which) -> {
                    dialog.dismiss();
                })
                .setCancelable(false).create();

        alertDialog.setCanceledOnTouchOutside(false);

        return alertDialog;
    }

    public SingleLiveEvent<Channel> getOnRemoveChannel() {
        return onRemoveChannel;
    }
}

