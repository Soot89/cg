package com.example.sandbox.crewgatherer.data.store;

import com.example.sandbox.crewgatherer.model.Channel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ChannelsRepository {
    private Dao<Channel, Integer> mDao;

    @Inject
    public ChannelsRepository(ConnectionSource connectionSource) {
        try {
            mDao = DaoManager.createDao(connectionSource, Channel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(Channel channel){
        try {
            mDao.create(channel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Channel channel){
        try {
            mDao.delete(channel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Channel> getChannelsForRoom(int roomID){
        try {
            return mDao.queryForEq("parentRoomID", roomID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
