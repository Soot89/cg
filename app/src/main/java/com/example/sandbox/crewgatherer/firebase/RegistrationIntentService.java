package com.example.sandbox.crewgatherer.firebase;

import com.google.firebase.iid.FirebaseInstanceId;

import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.di.modules.ConnectivityModule;
import com.example.sandbox.crewgatherer.utils.SharedPrefUtil;
import com.example.sandbox.crewgatherer.webservices.Webservice;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import javax.inject.Inject;

public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";
    @Inject
    protected Webservice mWebservice;
    @Inject
    SharedPrefUtil mSharedPrefUtil;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((CrewGathererApplication) getApplication())
                .mApplicationComponent
                .newContextComponent(new ConnectivityModule(this))
                .inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        String TAG = "HANDLER" ;
        String userName = intent.getStringExtra("USERNAME");


        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]

            String token =FirebaseInstanceId.getInstance().getToken();
            // [END get_token]
            Log.i(TAG, "Firebase Registration Token: " + token);


            if(token != null && (!mSharedPrefUtil.isTokenSentToServer())) {
                sendRegistrationToServer(token, userName);
                mSharedPrefUtil.setTokenSentToServer(true);
                mSharedPrefUtil.saveToken(token);
            }



            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            mSharedPrefUtil.setTokenSentToServer(false);
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);


    }

    private void sendRegistrationToServer(String token, String userName) {
        Log.i("IntentService", "sending regID");
        mWebservice.storeRegId(token, userName).subscribe(responseBodyResponse -> {},Throwable::printStackTrace);
    }
}
