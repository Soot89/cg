package com.example.sandbox.crewgatherer.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "channel")
public class Channel {
    @DatabaseField(id = true)
    private int channelID;
    @DatabaseField
    private String channelName;
    @DatabaseField
    private int parentRoomID;

    public Channel() {
    }

    public Channel(int channelID, String channelName, int parentRoomID) {
        this.channelID = channelID;
        this.channelName = channelName;
        this.parentRoomID = parentRoomID;
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getParentRoomID() {
        return parentRoomID;
    }

    public void setParentRoomID(int parentRoomID) {
        this.parentRoomID = parentRoomID;
    }
}
