package com.example.sandbox.crewgatherer.presentation;

import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.SendMessageDialogBinding;
import com.example.sandbox.crewgatherer.model.CrewMessage;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;

import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;

public class SendMessageDialog extends AppCompatDialogFragment {

    private SendMessageDialogBinding binding;
    private SingleLiveEvent<CrewMessage> messageSendSubject = new SingleLiveEvent<>();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.i("RegisterDialog", "OnCreateDialog");

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.send_message_dialog, null, false);
        AlertDialog alertDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                .setView(binding.getRoot())
                .setTitle("Wy�lij wiadomos�")
                .setPositiveButton("OK", null)
                .setCancelable(false)
                .create();

        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setOnShowListener(dialog -> {
            Button positive = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            positive.setOnClickListener((view) -> onOkButtonClick(dialog));
        });

        return alertDialog;
    }

    private void onOkButtonClick(DialogInterface dialogInterface) {
        String message = binding.message.getText().toString();
        messageSendSubject.postValue(new CrewMessage(message));
        dialogInterface.dismiss();
    }

    public SingleLiveEvent<CrewMessage> getMessageSendSubject() {
        return messageSendSubject;
    }
}
