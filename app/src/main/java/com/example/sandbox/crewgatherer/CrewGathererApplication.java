package com.example.sandbox.crewgatherer;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.example.sandbox.crewgatherer.di.ApplicationComponent;
import com.example.sandbox.crewgatherer.di.DaggerApplicationComponent;
import com.example.sandbox.crewgatherer.di.modules.ApplicationModule;
import com.facebook.stetho.Stetho;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class CrewGathererApplication extends MultiDexApplication {
    public ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        Stetho.initializeWithDefaults(this);

        // initalize Calligraphy
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Poppins-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
