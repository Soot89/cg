package com.example.sandbox.crewgatherer.webservices;

public class ChannelResponse extends BaseResponse{
    private String channelName;
    private int channelID;

    public ChannelResponse(String message) {
        super(message);
    }

    public String getChannelName() {
        return channelName;
    }

    public int getChannelID() {
        return channelID;
    }
}
