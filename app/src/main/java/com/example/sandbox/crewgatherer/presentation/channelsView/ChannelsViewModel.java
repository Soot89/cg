package com.example.sandbox.crewgatherer.presentation.channelsView;

import com.annimon.stream.Stream;
import com.example.sandbox.crewgatherer.data.store.ChannelsRepository;
import com.example.sandbox.crewgatherer.data.store.RoomRepository;
import com.example.sandbox.crewgatherer.model.Channel;
import com.example.sandbox.crewgatherer.model.ChannelRequest;
import com.example.sandbox.crewgatherer.model.Room;
import com.example.sandbox.crewgatherer.model.SendChannelMessage;
import com.example.sandbox.crewgatherer.model.SendMessageRequest;
import com.example.sandbox.crewgatherer.model.SendRoomMessage;
import com.example.sandbox.crewgatherer.utils.SharedPrefUtil;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;
import com.example.sandbox.crewgatherer.webservices.ChannelResponse;
import com.example.sandbox.crewgatherer.webservices.RoomRequest;
import com.example.sandbox.crewgatherer.webservices.Webservice;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ChannelsViewModel extends ViewModel {
    @NonNull
    private Webservice mWebservice;
    @NonNull
    private ChannelsRepository mChannelsRepository;
    @NonNull
    private SharedPrefUtil mSharedPrefUtil;
    @NonNull
    private RoomRepository mRoomRepository;

    private int roomID;
    private String roomName;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private SingleLiveEvent<ChannelResponse> mChannelResponseEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<List<Channel>> mChannelListEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Boolean> mIsRoomSubscribed = new SingleLiveEvent<>();


    @Inject
    public ChannelsViewModel(@NonNull Webservice webservice, @NonNull ChannelsRepository channelsRepository, @NonNull SharedPrefUtil sharedPrefUtil, @NonNull RoomRepository roomRepository) {
        mWebservice = webservice;
        this.mChannelsRepository = channelsRepository;
        this.mSharedPrefUtil = sharedPrefUtil;
        this.mRoomRepository = roomRepository;
    }

    public void onLoad(int roomID, String roomName) {
        this.roomID = roomID;
        this.roomName = roomName;
        mChannelListEvent.postValue(mChannelsRepository.getChannelsForRoom(roomID));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mCompositeDisposable.clear();
    }

    public void subscribeToChannel(String channelName) {
        mCompositeDisposable.add(mWebservice.subscribeToChannel(new ChannelRequest("SICRULEZ", mSharedPrefUtil.getUserName(), roomID, roomName, channelName, mSharedPrefUtil.getToken())).observeOn(Schedulers.computation()).subscribeOn(Schedulers.io())
                .subscribe(channelResponse -> {
                    if (channelResponse.getMessage() == null || channelResponse.getMessage().isEmpty()) {
                        mChannelsRepository.insert(new Channel(channelResponse.getChannelID(), channelResponse.getChannelName(), roomID));
                        mChannelListEvent.postValue(mChannelsRepository.getChannelsForRoom(roomID));
                    } else {
                        mChannelResponseEvent.postValue(channelResponse);
                    }
                }, Throwable::printStackTrace));
    }

    public void unscribeFromChannel(Channel channel) {
        mCompositeDisposable.add(mWebservice.unsubscribeFromChannel(new ChannelRequest("SICRULEZ", mSharedPrefUtil.getUserName(), roomID, roomName, channel.getChannelName(), mSharedPrefUtil.getToken())).observeOn(Schedulers.computation()).subscribeOn(Schedulers.io())
                .subscribe(channelResponse -> {
                    if (channelResponse.getMessage() == null || channelResponse.getMessage().isEmpty()) {
                        mChannelsRepository.delete(new Channel(channel.getChannelID(), channel.getChannelName(), roomID));
                        mChannelListEvent.postValue(mChannelsRepository.getChannelsForRoom(roomID));
                        Log.i("Unsubcribe", "Removed subscription");
                    }
                }, Throwable::printStackTrace));
    }

    public SingleLiveEvent<ChannelResponse> getChannelResponseEvent() {
        return mChannelResponseEvent;
    }

    public SingleLiveEvent<List<Channel>> getChannelListEvent() {
        return mChannelListEvent;
    }

    public void sendChannelMessage(SendChannelMessage sendChannelMessage) {
        Log.i("SendMs", "UserName: " + mSharedPrefUtil.getUserName());
        mCompositeDisposable.add(mWebservice.sendMessage(new SendMessageRequest(sendChannelMessage.getChannel().getChannelID(), roomID, false, sendChannelMessage.getMessage(), mSharedPrefUtil.getUserName(), "SICRULEZ"))
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.io())
                .subscribe(responseBodyResponse -> {
                    mChannelResponseEvent.postValue(new ChannelResponse("Wys�ano"));
                    Log.i("SendMsg", "Message sent");
                }, Throwable::printStackTrace));
    }

    public void sendRoomMessage(SendRoomMessage sendRoomMessage) {
        mCompositeDisposable.add(mWebservice.sendMessage(new SendMessageRequest(roomID, true, sendRoomMessage.getMessage(), mSharedPrefUtil.getUserName(), "SICRULEZ"))
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.io())
                .subscribe(responseBodyResponse -> {
                    mChannelResponseEvent.postValue(new ChannelResponse("Wys�ano"));
                    Log.i("SendMsg", "Message sent");
                }, Throwable::printStackTrace));
    }

    public void createChannel(String channelName) {
        mCompositeDisposable.add(mWebservice.createChannel(new ChannelRequest("SICRULEZ", mSharedPrefUtil.getUserName(), roomID, roomName, channelName, mSharedPrefUtil.getToken()))
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.io())
                .subscribe(channelResponse -> {
                    if (channelResponse.getMessage() != null && !channelResponse.getMessage().isEmpty()) {
                        mChannelResponseEvent.postValue(channelResponse);
                    }
                    mChannelResponseEvent.postValue(new ChannelResponse("Utworzono kana�"));
                    mChannelsRepository.insert(new Channel(channelResponse.getChannelID(), channelResponse.getChannelName(), roomID));
                    mChannelListEvent.postValue(mChannelsRepository.getChannelsForRoom(roomID));
                }));

    }

    public void subscribeToRoom() {
        mCompositeDisposable.add(mWebservice.subscribeToRoom(new RoomRequest("SICRULEZ", mSharedPrefUtil.getUserName(), roomID, roomName, mSharedPrefUtil.getToken())).observeOn(Schedulers.computation()).subscribeOn(Schedulers.io())
                .subscribe(channelResponse -> {
                    if (channelResponse.getMessage() == null || channelResponse.getMessage().isEmpty()) {
                        mRoomRepository.insert(new Room(roomID));
                    } else {
                        mChannelResponseEvent.postValue(channelResponse);
                    }
                }));
    }

    public void isRoomSubscribed() {
        boolean isRoomSubscribed = Stream.of(mRoomRepository.getSubscribedRooms()).anyMatch(room -> room.getRoomID() == roomID);
        mIsRoomSubscribed.postValue(isRoomSubscribed);
        //if is subscribed - show dialog to SendMessage else show dialog with question about subscribing. If Yes -> subsribeToRoom()
    }

    public SingleLiveEvent<Boolean> getIsRoomSubscribed() {
        return mIsRoomSubscribed;
    }
}
