package com.example.sandbox.crewgatherer.di.modules;


import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.utils.SharedPrefUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class  UtilityModule {
    @Singleton
    @Provides
    SharedPrefUtil providesSharedPrefUtil(CrewGathererApplication application) {
        return new SharedPrefUtil(application.getApplicationContext());
    }
}