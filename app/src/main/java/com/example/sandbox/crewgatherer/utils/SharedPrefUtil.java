package com.example.sandbox.crewgatherer.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtil {
    private static final String TOKEN_KEY = "token";
    private static final String USER_NAME_KEY= "username";
    private static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    private static final String REGISTRATION_COMPLETE = "registrationComplete";

    private SharedPreferences prefs;

    public SharedPrefUtil(Context context) {
        prefs = context.getSharedPreferences("crew", 0);
    }

    public void saveToken(String token) {
        prefs.edit().putString(TOKEN_KEY, token).apply();
    }

    public String getToken() {
        return prefs.getString(TOKEN_KEY, null);
    }

    public void saveUserName(String userName){
        prefs.edit().putString(USER_NAME_KEY, userName).apply();
    }

    public String getUserName() {
        return prefs.getString(USER_NAME_KEY, null);
    }

    public void setTokenSentToServer(boolean isSent){
        prefs.edit().putBoolean(SENT_TOKEN_TO_SERVER, isSent).apply();
    }

    public boolean isTokenSentToServer(){
        return prefs.getBoolean(SENT_TOKEN_TO_SERVER, false);
    }

    public void setRegistrationComplete(boolean isComplete){
        prefs.edit().putBoolean(REGISTRATION_COMPLETE, isComplete).apply();
    }

    public boolean isRegisitrationCompleted(){
        return prefs.getBoolean(REGISTRATION_COMPLETE, false);
    }
}