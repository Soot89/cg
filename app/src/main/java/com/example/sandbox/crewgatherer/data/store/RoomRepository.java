package com.example.sandbox.crewgatherer.data.store;

import com.example.sandbox.crewgatherer.model.Room;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RoomRepository {
    private Dao<Room, Integer> mDao;

    @Inject
    public RoomRepository(ConnectionSource connectionSource) {
        try {
            mDao = DaoManager.createDao(connectionSource, Room.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(Room room) {
        try {
            mDao.create(room);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Room room) {
        try {
            mDao.delete(room);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Room> getSubscribedRooms() {
        try {
            return mDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
