package com.example.sandbox.crewgatherer.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Log.i("FirebaseIDService", "Token refreshed");
        String token = FirebaseInstanceId.getInstance().getToken();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putString(QuickstartPreferences.TOKEN, token).apply();

        Intent registrationComplete = new Intent(QuickstartPreferences.TOKEN_RFRESHED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
}
