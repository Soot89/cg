package com.example.sandbox.crewgatherer.di.ContextScope;

import com.example.sandbox.crewgatherer.firebase.RegistrationIntentService;
import com.example.sandbox.crewgatherer.presentation.channelsView.ChannelsActivity;
import com.example.sandbox.crewgatherer.presentation.roomsView.RoomsListActivity;
import com.example.sandbox.crewgatherer.di.modules.ConnectivityModule;

import dagger.Subcomponent;


@ContextScope
@Subcomponent(modules = {ConnectivityModule.class})
public interface ContextComponent {
    void inject(RoomsListActivity roomsListActivity);
    void inject(RegistrationIntentService service);
    void inject(ChannelsActivity channelsActivity);
}