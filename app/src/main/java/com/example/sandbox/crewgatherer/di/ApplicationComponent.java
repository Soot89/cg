package com.example.sandbox.crewgatherer.di;

import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.di.ContextScope.ContextComponent;
import com.example.sandbox.crewgatherer.di.modules.ApplicationModule;
import com.example.sandbox.crewgatherer.di.modules.ConnectivityModule;
import com.example.sandbox.crewgatherer.di.modules.NetworkModule;
import com.example.sandbox.crewgatherer.di.modules.UtilityModule;
import com.example.sandbox.crewgatherer.di.modules.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, UtilityModule.class, ViewModelModule.class})
public interface ApplicationComponent {

    void inject(CrewGathererApplication app);

    ContextComponent newContextComponent(ConnectivityModule connectivityModule);

}
