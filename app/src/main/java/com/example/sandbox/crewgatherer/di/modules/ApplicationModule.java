package com.example.sandbox.crewgatherer.di.modules;

import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.data.store.DatabaseHelper;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class ApplicationModule {
    CrewGathererApplication app;

    public ApplicationModule(CrewGathererApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    CrewGathererApplication providesApplication() {
        return app;
    }

    @Provides
    @Singleton
    ConnectionSource providesConnectionSource(CrewGathererApplication app) {
        return new AndroidConnectionSource(new DatabaseHelper(app.getApplicationContext()));
    }
}
