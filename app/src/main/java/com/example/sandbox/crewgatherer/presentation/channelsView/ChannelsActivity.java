package com.example.sandbox.crewgatherer.presentation.channelsView;

import com.example.sandbox.crewgatherer.CrewGathererApplication;
import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.ChannelsListActivityBinding;
import com.example.sandbox.crewgatherer.di.modules.ConnectivityModule;
import com.example.sandbox.crewgatherer.model.SendChannelMessage;
import com.example.sandbox.crewgatherer.model.SendRoomMessage;
import com.example.sandbox.crewgatherer.presentation.RemoveSubscriptionDialog;
import com.example.sandbox.crewgatherer.presentation.SendMessageDialog;

import android.arch.lifecycle.ViewModelProvider;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Named;

public class ChannelsActivity extends AppCompatActivity {

    private final ChannelsAdapter mAdapter = new ChannelsAdapter();

    @Inject
    @Named("channels")
    ViewModelProvider.Factory viewModelFactory;

    private ChannelsViewModel viewModel;
    private ChannelsListActivityBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((CrewGathererApplication) getApplication())
                .mApplicationComponent
                .newContextComponent(new ConnectivityModule(this))
                .inject(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.channels_list_activity);
        viewModel = viewModelFactory.create(ChannelsViewModel.class);

        mBinding.parent.requestFocus();

        //get from roomsListActivity clickedRoomID
        int roomID = getIntent().getIntExtra("RoomID", -1);
        String roomName = getIntent().getStringExtra("RoomName");

        getSupportActionBar().setTitle(roomName);

        mBinding.channelsList.setAdapter(mAdapter);
        mBinding.channelsList.setLayoutManager(new LinearLayoutManager(this));

        viewModel.onLoad(roomID, roomName);

        mBinding.btnJoin.setOnClickListener(v -> {
            if (mBinding.etSearchView.getText().toString().isEmpty()) {
                mBinding.etSearchView.setError("Pusta nazwa");
            } else {
                viewModel.subscribeToChannel(mBinding.etSearchView.getText().toString());
            }
        });

        mBinding.sendRoomMsg.setOnClickListener(v -> viewModel.isRoomSubscribed());

        mBinding.addChannel.setOnClickListener(v -> {
            CreateChannelDialog createChannelDialog = new CreateChannelDialog();
            createChannelDialog.getChannelCreateEvent().observe(this, channelName -> viewModel.createChannel(channelName));
            createChannelDialog.show(getSupportFragmentManager(), "CreateChannel");
        });

        viewModel.getChannelResponseEvent().observe(this, channelResponse -> Toast.makeText(this, channelResponse.getMessage(), Toast.LENGTH_SHORT).show());

        viewModel.getChannelListEvent().observe(this, mAdapter::setItems);

        mAdapter.getOnChannelClickSubject().observe(this, channel -> {
            SendMessageDialog sendMessageDialog = new SendMessageDialog();
            sendMessageDialog.getMessageSendSubject().observe(this, crewMessage -> viewModel.sendChannelMessage(new SendChannelMessage(channel, crewMessage.getMessage())));
            sendMessageDialog.show(getSupportFragmentManager(), "SendMsg");
        });

        mAdapter.getOnChannelRemoveSubClickSubject().observe(this, channel -> {
            RemoveSubscriptionDialog removeSubscriptionDialog = new RemoveSubscriptionDialog();
            removeSubscriptionDialog.setSelectedChannel(channel);
            removeSubscriptionDialog.getOnRemoveChannel().observe(this, channelToRemove -> viewModel.unscribeFromChannel(channelToRemove));
            removeSubscriptionDialog.show(getSupportFragmentManager(), "RemSub");
        });

        viewModel.getIsRoomSubscribed().observe(this, isSubscribed -> {
            if (isSubscribed) {
                SendMessageDialog sendMessageDialog = new SendMessageDialog();
                sendMessageDialog.getMessageSendSubject().observe(this, crewMessage -> viewModel.sendRoomMessage(new SendRoomMessage(crewMessage.getMessage())));
                sendMessageDialog.show(getSupportFragmentManager(), "SendMsg");
            } else {
                new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                        .setTitle("Subskrypcja do pokoju")
                        .setMessage("Nie jeste� zapisany do pokoju. Czy chcesz to zrobi� teraz?")
                        .setPositiveButton("Tak", (dialog, which) -> {
                            viewModel.subscribeToRoom();
                            dialog.dismiss();
                        })
                        .setNegativeButton("Nie", (dialog, which) -> dialog.dismiss())
                        .create().show();
            }
        });
    }


}
