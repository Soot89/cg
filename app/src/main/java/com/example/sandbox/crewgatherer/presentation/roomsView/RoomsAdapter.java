package com.example.sandbox.crewgatherer.presentation.roomsView;

import com.bumptech.glide.Glide;
import com.example.sandbox.crewgatherer.OnDoubleTapListener;
import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.RoomListItemBinding;
import com.example.sandbox.crewgatherer.model.Room;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class RoomsAdapter extends RecyclerView.Adapter<RoomsAdapter.RoomViewHolder> {
    private List<Room> itemsList = new ArrayList<>();

    private SingleLiveEvent<Room> onRoomClickListener = new SingleLiveEvent<>();
    private SingleLiveEvent<Room> onRoomDoubleClickListener = new SingleLiveEvent<>();

    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RoomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
        Room room = itemsList.get(position);
        holder.mSelectedRoom = room;
        holder.mBinding.roomName.setText(room.getRoomTitle());
        Glide.with(holder.itemView.getContext()).load(room.getRoomPictureUrl()).into(holder.mBinding.roomPicture);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {
        private Room mSelectedRoom;
        @NonNull
        private final RoomListItemBinding mBinding;

        RoomViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            itemView.setOnTouchListener(new OnDoubleTapListener(itemView.getContext()) {
                @Override
                public void onDoubleTap(MotionEvent e) {
                    onRoomDoubleClickListener.postValue(mSelectedRoom);
                }

                @Override
                public void onSingleTap(MotionEvent e) {
                    onRoomClickListener.postValue(mSelectedRoom);
                }
            });
        }
    }

    public void setItemsList(List<Room> itemsList) {
        this.itemsList = itemsList;
    }

    public SingleLiveEvent<Room> getOnRoomClickListenerSubject() {
        return onRoomClickListener;
    }

    public SingleLiveEvent<Room> getOnRoomDoubleClickListenerSubject() {
        return onRoomDoubleClickListener;
    }
}
