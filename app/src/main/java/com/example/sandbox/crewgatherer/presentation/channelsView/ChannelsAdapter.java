package com.example.sandbox.crewgatherer.presentation.channelsView;

import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.ChannelsListItemBinding;
import com.example.sandbox.crewgatherer.model.Channel;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.ChannelsViewHolder> {
    private List<Channel> channelsList = new ArrayList<>();

    private SingleLiveEvent<Channel> onChannelClickSubject = new SingleLiveEvent<>();
    private SingleLiveEvent<Channel> onChannelRemoveSubClickSubject = new SingleLiveEvent<>();

    @NonNull
    @Override
    public ChannelsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChannelsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.channels_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelsViewHolder holder, int position) {
        Channel channel = channelsList.get(position);
        holder.mSelectedChannel = channel;
        holder.mBinding.channelName.setText(channel.getChannelName());
    }

    @Override
    public int getItemCount() {
        return channelsList.size();
    }

    public void setItems(List<Channel> channelsList) {
        this.channelsList = channelsList;
        notifyDataSetChanged();
    }


    class ChannelsViewHolder extends RecyclerView.ViewHolder {
        private ChannelsListItemBinding mBinding;
        private Channel mSelectedChannel;

        public ChannelsViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            assert mBinding != null;
            mBinding.channelName.setOnClickListener(v -> onChannelClickSubject.postValue(mSelectedChannel));
            mBinding.removeSubscription.setOnClickListener(v -> onChannelRemoveSubClickSubject.postValue(mSelectedChannel));
        }
    }

    public SingleLiveEvent<Channel> getOnChannelClickSubject() {
        return onChannelClickSubject;
    }

    public SingleLiveEvent<Channel> getOnChannelRemoveSubClickSubject() {
        return onChannelRemoveSubClickSubject;
    }
}
