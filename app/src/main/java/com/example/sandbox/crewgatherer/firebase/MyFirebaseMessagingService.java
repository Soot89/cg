package com.example.sandbox.crewgatherer.firebase;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.example.sandbox.crewgatherer.BuildConfig;
import com.example.sandbox.crewgatherer.R;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String CHANNEL_ID = "3153151";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i("MESSAGE", "from: " + remoteMessage.getData().get("sender"));
        Log.i("MESSAGE", "msg: " + remoteMessage.getData().get("message"));
        Log.i("MESSAGE", "roomName: " + remoteMessage.getData().get("roomName"));
        Log.i("MESSAGE", "channelName: " + remoteMessage.getData().get("channelName"));
        super.onMessageReceived(remoteMessage);

        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();

        //TODO rozróznianie ikonki powiadomienia na podstawie nazwy pokoju

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = new Random().nextInt((20 - 1) + 1) + 1;

        String title = remoteMessage.getData().get("channelName") + "(" + remoteMessage.getData().get("roomName") + ")";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setPriority((Build.VERSION.SDK_INT > Build.VERSION_CODES.O) ? NotificationManager.IMPORTANCE_HIGH : NotificationCompat.PRIORITY_HIGH )
                .setSmallIcon(android.R.drawable.ic_menu_call)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("message") + "\n" + "OD: " +  remoteMessage.getData().get("sender")))
                .setContentText(remoteMessage.getData().get("message") + "\n" + "OD: " +  remoteMessage.getData().get("sender"))
                .setVibrate(new long[]{1000, 2000, 1000, 3000, 1000, 4000, 2000})
                .setLights(Color.RED, 3000, 3000)
                .setSound(defaultSoundUri).build();

        mNotificationManager.notify(randomNum, notification);
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
