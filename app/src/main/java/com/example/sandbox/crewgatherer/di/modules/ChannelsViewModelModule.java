package com.example.sandbox.crewgatherer.di.modules;

import com.example.sandbox.crewgatherer.ViewModelUtil;
import com.example.sandbox.crewgatherer.presentation.channelsView.ChannelsViewModel;

import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ChannelsViewModelModule {
    @Singleton
    @Provides
    @Named("channels")
    static ViewModelProvider.Factory provideViewModelProviderFactory(ViewModelUtil viewModelUtil, ChannelsViewModel viewModel) {
        return viewModelUtil.createFor(viewModel);
    }
}
