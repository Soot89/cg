package com.example.sandbox.crewgatherer.presentation.channelsView;

import com.example.sandbox.crewgatherer.R;
import com.example.sandbox.crewgatherer.databinding.CreateChannelDialogBinding;
import com.example.sandbox.crewgatherer.utils.SingleLiveEvent;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;

public class CreateChannelDialog extends AppCompatDialogFragment {
    private CreateChannelDialogBinding binding;
    private SingleLiveEvent<String> channelCreateEvent = new SingleLiveEvent();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.create_channel_dialog, null, false );

        return new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                .setTitle("Utw�rz kana�")
                .setView(binding.getRoot())
                .setPositiveButton("Utw�rz", (dialog, which) -> {
                    String channelName = binding.channelName.getText().toString();
                    if (!channelName.isEmpty()) {
                        channelCreateEvent.postValue(channelName);
                        dialog.dismiss();
                    } else {
                        binding.channelName.setError("Pusta nazwa");
                    }
                })
                .setNegativeButton("Anuluj", (dialog, which) -> {
                    dialog.dismiss();
                })
                .create();
    }

    public SingleLiveEvent<String> getChannelCreateEvent() {
        return channelCreateEvent;
    }
}
