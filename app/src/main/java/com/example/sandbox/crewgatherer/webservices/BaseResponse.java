package com.example.sandbox.crewgatherer.webservices;

public class BaseResponse {
    private String message;
    private String error;

    public BaseResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }
}
